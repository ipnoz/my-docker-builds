<?php

/**
 * phpMyAdmin configuration document:
 *
 * @see https://docs.phpmyadmin.net/fr/latest/config.html
 */

$i = 1;
$servers = [];

$servers[$i] = [
    'name' => 'Dev',
    'host' => '172.17.0.1',
    'port' => 3306,
    'user' => 'root',
    'password' => '',
];
$servers[++$i] = [
    'name' => 'DDS',
    'host' => '172.17.0.1',
    'port' => 3307,
    'user' => 'root',
    'password' => '',
];

foreach ($servers as $i => $server) {
    $cfg['Servers'][$i]['verbose'] = $server['name'];
    $cfg['Servers'][$i]['host'] = $server['host'];
    $cfg['Servers'][$i]['port'] = $server['port'];
    $cfg['Servers'][$i]['connect_type'] = 'tcp';
    $cfg['Servers'][$i]['extension'] = 'mysqli';
    $cfg['Servers'][$i]['auth_type'] = 'config';
    $cfg['Servers'][$i]['user'] = $server['user'];
    $cfg['Servers'][$i]['password'] = $server['password'];
    $cfg['Servers'][$i]['AllowNoPassword'] = true;
}

$cfg['LoginCookieValidity'] = 60*60*24*7*365*30; # 30 ans
$cfg['LoginCookieValidityDisableWarning'] = true;
