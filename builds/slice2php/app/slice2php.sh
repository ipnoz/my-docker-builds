#!/bin/bash

#
# Convert all versions of Murmur slices (Murmur.ice) for PHP
#

declare -a ICE_VERSIONS=("3.4.2" "3.5.1" "3.6.3" "3.7.2");

#readonly STRUCT="i386"
readonly STRUCT="amd64"

readonly SLICE2PHP="bin/${STRUCT}/slice2php"
readonly MURMUR_SLICES_DIR="MurmurIce/"
readonly CHECKSUMFILE="Ice/SliceChecksumDict.ice"

readonly OUTPUTDIR=".output/"

#
# Process
#

cd `dirname $0`

[ ! -d $OUTPUTDIR ] && mkdir $OUTPUTDIR && chmod 755 $OUTPUTDIR

for iceversion in ${ICE_VERSIONS[*]}; do

   ICEDIR=${OUTPUTDIR}"ice-"$iceversion

   [ ! -d $ICEDIR ] && mkdir $ICEDIR && chmod 755 $ICEDIR

    cp ${CHECKSUMFILE}"_"$iceversion ${CHECKSUMFILE}

    for slice in `ls $MURMUR_SLICES_DIR`; do
        ${SLICE2PHP}"_"${iceversion} -I "./" --output-dir $ICEDIR --all --ice $MURMUR_SLICES_DIR$slice --checksum
    done
done
