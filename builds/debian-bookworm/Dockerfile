
# SYSTEM
FROM debian:bookworm

ENV DOCKER_DEBIAN_NAME="bookworm"

# Priority to ipv4, Docker have lot of problem with ipv6
COPY ./etc/gai.conf /etc/

RUN echo "deb http://deb.debian.org/debian ${DOCKER_DEBIAN_NAME} main contrib" > /etc/apt/sources.list
RUN echo "deb http://deb.debian.org/debian ${DOCKER_DEBIAN_NAME}-updates main contrib" > /etc/apt/sources.list
RUN echo "deb http://deb.debian.org/debian-security ${DOCKER_DEBIAN_NAME}-security main contrib" >> /etc/apt/sources.list
RUN echo "APT::Install-Recommends \"false\";" > /etc/apt/apt.conf.d/999-ipnoz
RUN echo "APT::Install-Suggests \"false\";" >> /etc/apt/apt.conf.d/999-ipnoz

RUN apt update
RUN apt upgrade -y
RUN apt clean && apt autoclean && apt -y autoremove

RUN apt install -y vim locales net-tools procps curl wget ca-certificates \
    apt-transport-https lsb-release gnupg2 esmtp-run poppler-utils

# Paris timezone
RUN rm -f /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN echo "Europe/Paris" > /etc/timezone

# Debian en francais + UTF8
RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen fr_FR.UTF-8
RUN echo "LANG=fr_FR.UTF-8" > /etc/environment

# MDP pour l'utilisateur root (pass=root)
RUN usermod -p '$6$cOUovefk$dJT/Gq13snCnT3KSyXA7f7QKrjOgelD5K/G15sLnSZgmAIQgJti0RmGOzzVnlJiM.89uoS9tpSx7VElRVouLt1' root

# Add dav user and group to system
RUN groupadd -g 1000 dav
RUN useradd --create-home --system --shell /bin/bash -u 1000 -g 1000 dav

# Copy custom bash file
COPY ./etc/bash.ipnoz /etc/
COPY ./etc/vimrc.local /etc/vim/

# Source the custom bash configuration
RUN echo "source /etc/bash.ipnoz" > /root/.bashrc
RUN echo "source /etc/bash.ipnoz" > /home/dav/.bashrc

# ambiguous TAB
RUN echo "set show-all-if-ambiguous on" >> /etc/inputrc
